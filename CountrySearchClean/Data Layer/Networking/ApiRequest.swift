//
//  ApiRequest.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

struct ApiRequest{
    let baseURLString = "https://restcountries.eu/rest/v2/"
    let endpoint: Endpoint
    
    init(endpoint: Endpoint){
        self.endpoint = endpoint
    }
    
    var urlRequest: URLRequest? {
        let URLString = baseURLString + endpoint.parameters
        guard let url = URL(string: URLString) else {return nil}
        return URLRequest(url: url)
    }
}

enum RequestError: Error{
    case urlRequestFailed
}
