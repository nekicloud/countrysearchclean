//
//  ImageRequest.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class FlagImageRequest{
    let baseURL = "https://flagpedia.net/data/flags/normal/"
    let countryCode: String
    
    init(countryCode: String){
        self.countryCode = countryCode
    }
    
    var urlRequest: URLRequest? {
        let URLString = baseURL + countryCode + ".png"
        guard let url = URL(string: URLString) else {return nil}
        return URLRequest(url: url)
    }
}
