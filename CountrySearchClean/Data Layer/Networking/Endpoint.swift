//
//  Endpoint.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

enum Endpoint {
    case countryNameSearch(name: String)
    case countryCapitalSearch(capital: String)
    case countryLanguageSearch(language: String)
    case allCountries()
    
    var parameters: String{
        switch self{
        case let .countryNameSearch(name: name):
            return "name/\(name)"
        case let .countryCapitalSearch(capital: capital):
            return "capital/\(capital)"
        case let .countryLanguageSearch(language: language):
            return "lang/\(language)"
        case .allCountries():
            return "all"
        }
    }
}
