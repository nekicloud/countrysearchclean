//
//  RCNameSearchProvider.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class RCNameSearchProvider: CountryNameSearchProvider{
    let webService: WebService
    
    init(webService: WebService = WebServiceProvider(session: DataNetworksession())){
        self.webService = webService
    }
    
    func fetchCountry(for name: String, completion: @escaping (Response<Countries>) -> Void) {
        guard let request = ApiRequest(endpoint: .countryNameSearch(name: name)).urlRequest else {return}
        webService.execute(request){ (response: Response<[SearchResponseData]>) in
            switch response{
            case .success(let countries):
                completion(.success(self.getCountryList(result: countries)))
            case .error(let error):
                print(error)
            }
        }
    }
    
    private func getCountryList(result: [SearchResponseData]) -> Countries{
        var countries = Countries()
        result.forEach{ country in
            let nextCountry = RCCountry(
             name: country.name,
             countryCode: country.alpha2Code,
             population: country.population,
             capitalCity: country.capital,
             region: country.region,
             regionBlocks: country.regionalBlocs.first?.acronym ?? "",
             language: country.languages.first?.name ?? "",
             currency: country.currencies.first?.code ?? "",
             coordinates: Coordinates(lat: country.latlng.first ?? 0, lon: country.latlng.last ?? 0))
             countries.append(nextCountry)
        }
        return countries
    }
}

struct RCCountry: Country{
    var name: String
    var countryCode: String
    var population: Int
    var capitalCity: String
    var region: String
    var regionBlocks: String
    var language: String
    var currency: String
    var coordinates: Coordinates
    
    init(name: String, countryCode: String, population: Int, capitalCity: String, region: String, regionBlocks: String, language: String, currency: String, coordinates: Coordinates) {
        self.name = name
        self.countryCode = countryCode
        self.population = population
        self.capitalCity = capitalCity
        self.region = region
        self.regionBlocks = regionBlocks
        self.language = language
        self.currency = currency
        self.coordinates = coordinates
    }
}

struct SearchResponseData: Codable {
    let name: String
    let alpha2Code: String
    let capital: String
    let region, subregion: String
    let population: Int
    let latlng: [Double]
    let currencies: [Currency]
    let languages: [Language]
    let regionalBlocs: [RegionalBloc]
}

struct Currency: Codable {
    let code: String?
    let symbol: String?
}

struct Language: Codable {
    let name, nativeName: String
    
    enum CodingKeys: String, CodingKey {
        case name, nativeName
    }
}

struct RegionalBloc: Codable {
    let acronym, name: String
}

struct Translations: Codable {
    let de, es, fr, ja: String
    let it, br, pt, nl: String
    let hr, fa: String
}
