//
//  RCLanguageSearchProvider.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class RCLanguageSearchProvider: CountryLanguageSearchProvider{
    let webService: WebService
    
    init(webService: WebService = WebServiceProvider(session: DataNetworksession())){
        self.webService = webService
    }
    
    func fetchCountry(for language: String, completion: @escaping (Response<Countries>) -> Void) {
        guard let request = ApiRequest(endpoint: .countryLanguageSearch(language: language)).urlRequest else {return}
        webService.execute(request){ (response: Response<[SearchResponseData]>) in
            switch response{
            case .success(let countries):
                completion(.success(self.getCountryList(result: countries)))
            case .error(let error):
                print(error)
            }
        }
    }
    
    private func getCountryList(result: [SearchResponseData]) -> Countries{
        var countries = Countries()
        result.forEach{ country in
            let nextCountry = RCCountry(
                name: country.name,
                countryCode: country.alpha2Code,
                population: country.population,
                capitalCity: country.capital,
                region: country.region,
                regionBlocks: country.regionalBlocs.first?.acronym ?? "",
                language: country.languages.first?.name ?? "",
                currency: country.currencies.first?.code ?? "",
                coordinates: Coordinates(lat: country.latlng.first ?? 0, lon: country.latlng[1]))
            countries.append(nextCountry)
        }
        return countries
    }
}
