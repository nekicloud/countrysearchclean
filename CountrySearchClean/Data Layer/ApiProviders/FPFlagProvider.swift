//
//  FPFlagProvider.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class FPFlagProvider: FlagProvider{
    let webService: WebService
    var flagCache: [String:Data] = [:]
    
    init(webService: WebService = WebServiceProvider(session: DataNetworksession())){
        self.webService = webService
    }
    
    func fetchFlag(for countryCode: String, completion: @escaping (Response<Data>) -> Void) {
        if let flagData = flagCache[countryCode] {
            completion(.success(flagData))
        }
        guard let imageRequest = FlagImageRequest(countryCode: countryCode.lowercased()).urlRequest else{ return}
        webService.execute(imageRequest) { [weak self] (response: Response<Data>) in
            switch response{
            case .success(let imageData):
                self?.flagCache[countryCode] = imageData
                completion(.success(imageData))
            case .error(let error):
                print(error)
            }
        }
    }
}
