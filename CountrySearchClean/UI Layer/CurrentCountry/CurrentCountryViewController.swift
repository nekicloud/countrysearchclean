//
//  CountryViewController.swift
//  CountrySearchClean
//
//  Created by Luka on 30/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class CurrentCountryViewController: UIViewController{
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var country: Country
    var dataSource: CountryVCDataSource?
    let flag: UIImage
    
    init(country: Country, flag: UIImage){
        self.country = country
        self.flag = flag
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {return nil}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        flagImage.image = flag
        countryLbl.text = country.name
        dataSource = CountryVCDataSource(country: country)
        setUpTableView()
    }
    
    func setUpTableView(){
        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

extension CurrentCountryViewController: UITableViewDelegate{}
