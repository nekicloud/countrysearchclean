//
//  CountryVCDataSource.swift
//  CountrySearchClean
//
//  Created by Luka on 30/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol CurrentCountryUpdateFeedback{
    func dataSourceUpdated()
}

class CountryVCDataSource: NSObject{
    let countryData: [String]
    
    init(country: Country){
        self.countryData = [
            "Population: \(country.population)",
            "Capital: \(country.capitalCity)",
            "Region: \(country.region)",
            "Region block Acronym: \(country.regionBlocks)",
            "Language: \(country.language)",
            "Currency: \(country.currency)"
        ]
    }
}

extension CountryVCDataSource: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = countryData[indexPath.row]
        return cell
    }
}
