//
//  AppCoordinator.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class AppCoordinator{
    var window: UIWindow
    private var emptyView: UIView?
    var dependencies = AppDependenciesContainer()
    var homeVC: CountrySearchViewController?
    
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds)){
        self.window = window
    }
    
    func start(){
        homeVC = CountrySearchViewController(dataSource: CountrySearchVCDataSource(dependencies: dependencies))
        homeVC?.searchDelegate = self
        homeVC?.countryDelegate = self
        window.rootViewController = homeVC
        window.makeKeyAndVisible()
    }
}

extension AppCoordinator: SearchDelegate{
    func userAskedForSearch(with term: String, type: String, currentCountry: Country, flagImage: UIImage) {
        let resultVC = CountrySearchResultViewController(searchTerm: term, searchType: type, myCountry: currentCountry, flag: flagImage, dataSource: CountrySearchResultVCDataSource(dependencies: dependencies))
        resultVC.countryDelegate = self
        homeVC?.present(resultVC, animated: true, completion: nil)
    }
}

extension AppCoordinator: CurrentCountryDelegate{
    func userClickedFlag(country: Country, flag: UIImage){
        let countryVC = CurrentCountryViewController(country: country, flag: flag)
        homeVC?.present(countryVC,animated: true, completion: nil)
    }
}
