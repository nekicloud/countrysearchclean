//
//  CountrySearchResultVCDataSource.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol ResultVCDataSourceDependencies{
    var languageSearchUseCase: CountryLanguageSearchUseCase{get}
    var capitalSearchUseCase: CountryCapitalSearchUseCase{get}
    var nameSearchUseCase: CountryNameSearchUseCase{get}
    var flagFetcher: FlagImageUseCase{get}
}

protocol ResultVCDataSourceUpdateFeedback: class {
    func dataSourceUpdated()
}

class CountrySearchResultVCDataSource: NSObject{
    let cellID = "countryCell"
    let dependencies: ResultVCDataSourceDependencies
    var countries = Countries()
    weak var feedback: ResultVCDataSourceUpdateFeedback?
    
    init(dependencies: ResultVCDataSourceDependencies){
        self.dependencies = dependencies
    }
    
    func fetchByLanguage(searchTerm: String){
        dependencies.languageSearchUseCase.fetchCountry(for: searchTerm){[weak self] response in
            switch response{
            case .success(let countries):
                self?.countries = countries
                self?.feedback?.dataSourceUpdated()
            case .error(let error):
                print(error)
            }
        }
    }
    
    func fetchByName(searchTerm: String){
        dependencies.nameSearchUseCase.fetchCountry(for: searchTerm){[weak self] response in
            switch response{
            case .success(let countries):
                self?.countries = countries
                self?.feedback?.dataSourceUpdated()
            case .error(let error):
                print(error)
            }
        }
    }
    
    func fetchByCapital(searchTerm: String){
        dependencies.capitalSearchUseCase.fetchCountry(for: searchTerm){[weak self] response in
            switch response{
            case .success(let countries):
                self?.countries = countries
                self?.feedback?.dataSourceUpdated()
            case .error(let error):
                print(error)
            }
        }
    }
    
    func fetchFlags(country: Country, cell: CountryResultCell){
        dependencies.flagFetcher.fetchFlag(for: country.countryCode){ response in
            switch response{
            case .success(let image):
                let image = UIImage(data: image.image)
                cell.flagImage.image = image
            case .error(let error):
                print(error)
            }
        }
    }
}

extension CountrySearchResultVCDataSource: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        guard let countryResultCell = cell as? CountryResultCell else { return cell}
        countryResultCell.countryLbl.text = countries[indexPath.row].name
        countryResultCell.popLbl.text = "\(countries[indexPath.row].population)"
        fetchFlags(country: countries[indexPath.row], cell: countryResultCell)
        return countryResultCell
    }
}
