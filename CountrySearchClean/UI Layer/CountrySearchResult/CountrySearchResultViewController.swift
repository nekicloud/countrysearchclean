//
//  CountrySearchResultViewController.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class CountrySearchResultViewController: UIViewController {
    @IBOutlet weak var currentCountryButton: UIButton!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    weak var countryDelegate: CurrentCountryDelegate?
    let searchTerm: String
    let searchType: String
    let dataSource: CountrySearchResultVCDataSource
    let myCountry: Country
    let flagImage: UIImage
    
    
    init(searchTerm: String, searchType:String, myCountry: Country, flag: UIImage, dataSource: CountrySearchResultVCDataSource) {
        self.searchTerm = searchTerm
        self.searchType = searchType
        self.dataSource = dataSource
        self.myCountry = myCountry
        self.flagImage = flag
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) { return nil }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.countryLbl.text = myCountry.name
        self.currentCountryButton.setImage(flagImage, for: .normal)
        self.currentCountryButton.setImage(flagImage, for: .selected)
        self.currentCountryButton.imageView?.contentMode = .scaleAspectFill
        setUpTableView()
        fetchData()
    }
    
    func setUpTableView(){
        tableView.register(UINib(nibName: "CountryResultCell", bundle: nil), forCellReuseIdentifier: "countryCell")
        tableView.delegate = self
        tableView.dataSource = dataSource
        dataSource.feedback = self
        tableView.separatorStyle = .none
    }
    
    func fetchData(){
        switch searchType {
        case "Name":
            dataSource.fetchByName(searchTerm: searchTerm)
        case "Lang":
            dataSource.fetchByLanguage(searchTerm: searchTerm)
        case "City":
            dataSource.fetchByCapital(searchTerm: searchTerm)
        default:
            return
        }
    }
    
    @IBAction func countryBtnPressed(_ sender: UIButton) {
         countryDelegate?.userClickedFlag(country: myCountry, flag: currentCountryButton.imageView?.image ?? UIImage())
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CountrySearchResultViewController: UITableViewDelegate{}

extension CountrySearchResultViewController: ResultVCDataSourceUpdateFeedback{
    func dataSourceUpdated(){
        tableView.reloadData()
    }
}
