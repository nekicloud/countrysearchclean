//
//  CountryResultCell.swift
//  CountrySearchClean
//
//  Created by Luka on 30/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class CountryResultCell: UITableViewCell{
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var popLbl: UILabel!
    @IBOutlet weak var areaLbl: UILabel!
}
