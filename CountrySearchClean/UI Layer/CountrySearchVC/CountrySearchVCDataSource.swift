//
//  CountrySearchVCDataSource.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol CountrySearchVCDependencies{
    var allCountriesUseCase: CountriesUseCase{get}
    var countriesSorter: CountriesSorterUseCase{get}
    var flagFetcher: FlagImageUseCase{get}
}

protocol CountrySearchVCUpdateFeedback: class{
    func dataSourceUpdated()
    func updateCountry(country: Country, image: UIImage)
}

class CountrySearchVCDataSource: NSObject{
    let cellID = "countryCell"
    let dependencies: CountrySearchVCDependencies
    var countries = Countries()
    weak var feedback: CountrySearchVCUpdateFeedback?
    
    init(dependencies: CountrySearchVCDependencies){
        self.dependencies = dependencies
    }
    
    func fetchData(){
        dependencies.allCountriesUseCase.fetchCountriesByDistance() { [weak self] response in
            switch response{
            case .success(let countries):
                self?.countries = countries
                self?.sortCountries()
            case .error(let error):
                print(error)
            }
        }
    }
    
    func sortCountries(){
        countries = dependencies.countriesSorter.sortCountries(countriesToSort: countries)
        guard let safeCountry = countries.first else{ return}
        setCurrentCountry(country: safeCountry)
        countries.removeFirst()
        feedback?.dataSourceUpdated()
    }
    
    func setCurrentCountry(country: Country){
        dependencies.flagFetcher.fetchFlag(for: country.countryCode) {[weak self] response in
            switch response{
            case .success(let image):
                guard let flag = UIImage(data: image.image) else {return}
                self?.feedback?.updateCountry(country: country, image: flag)
            case .error(let error):
                print(error)
            }
        }
    }
    
    func fetchFlags(country: Country, cell: CountryCell){
        dependencies.flagFetcher.fetchFlag(for: country.countryCode){ response in
            switch response{
            case .success(let image):
                let image = UIImage(data: image.image)
                cell.flag.image = image
            case .error(let error):
                print(error)
            }
        }
    }
}

extension CountrySearchVCDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        guard let countryCell = cell as? CountryCell else { return cell}
        countryCell.countryNameLbl.text = countries[indexPath.row].name
        fetchFlags(country: countries[indexPath.row], cell: countryCell)
        return countryCell
    }
}
