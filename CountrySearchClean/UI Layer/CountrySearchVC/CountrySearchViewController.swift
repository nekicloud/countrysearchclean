//
//  ViewController.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import DownPicker

protocol SearchDelegate: class {
    func userAskedForSearch(with term: String, type: String, currentCountry: Country, flagImage: UIImage)
}

protocol CurrentCountryDelegate: class{
    func userClickedFlag(country: Country, flag: UIImage)
}

class CountrySearchViewController: UIViewController {
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var currentCountryButton: UIButton!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dropDownTextField: UITextField!
    var dropDownMenu: DownPicker!
    var myCountry: Country?
    let dataSource: CountrySearchVCDataSource
    weak var searchDelegate: SearchDelegate?
    weak var countryDelegate: CurrentCountryDelegate?
    let sharedDefaults = UserDefaults(suiteName: "group.luka.sharedDefaults")
    
    init(dataSource:CountrySearchVCDataSource){
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {return nil}
    

    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        setUpTableView()
        setUpDropDown()
        dataSource.fetchData()
    }
    
    func dataReceived(notification: NSNotification) {
        //deal with notification.userInfo
    }
    
    private func setUpDropDown(){
        let choices = ["Name","Lang","City"]
        dropDownMenu = DownPicker(textField: dropDownTextField, withData: choices)
        dropDownMenu.selectedIndex = 0
    }
    
    private func setUpTableView(){
        tableView.register(UINib(nibName: "CountryCell", bundle: nil), forCellReuseIdentifier: "countryCell")
        tableView.delegate = self
        tableView.dataSource = dataSource
        dataSource.feedback = self
        tableView.separatorStyle = .none
    }
    
    @IBAction func countryBtnPressed(_ sender: UIButton) {
        guard let safeCountry = myCountry else {return}
        countryDelegate?.userClickedFlag(country: safeCountry, flag: currentCountryButton.imageView?.image ?? UIImage())
    }
    
    @IBAction func goBtnPressed(_ sender: Any) {
        if searchTextField.text != "" {
        guard let safeText = searchTextField.text else {return}
        searchDelegate?.userAskedForSearch(with: safeText, type: dropDownMenu.text, currentCountry: myCountry!, flagImage: (currentCountryButton.imageView?.image)!)
        }
    }
}

extension CountrySearchViewController: UITableViewDelegate{}

extension CountrySearchViewController: UITextFieldDelegate{
}

extension CountrySearchViewController:CountrySearchVCUpdateFeedback{
    func dataSourceUpdated(){
        tableView.reloadData()
    }
    func updateCountry(country: Country, image: UIImage) {
        currentCountryButton.setImage(image, for: .normal)
        currentCountryButton.setImage(image, for: .selected)
        currentCountryButton.imageView?.contentMode = .scaleAspectFill
        countryLbl.text = country.countryCode
        self.myCountry = country
        sharedDefaults!.set("\(country.name)", forKey: "country")
        sharedDefaults!.set("\(country.countryCode)", forKey: "countryCode")
    }
}


