//
//  CountryCell.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell{
    @IBOutlet weak var flag: UIImageView!
    @IBOutlet weak var countryNameLbl: UILabel!
}
