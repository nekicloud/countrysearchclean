//
//  FlagProvider.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol FlagProvider{
    func fetchFlag(for countryCode: String, completion: @escaping(Response<Data>) -> Void)
}
