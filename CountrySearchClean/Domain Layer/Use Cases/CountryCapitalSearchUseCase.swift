//
//  CountryCapitalSearchUseCase.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol CountryCapitalSearchUseCase {
    func fetchCountry(for capital: String, completion: @escaping(Response<Countries>) -> Void)
}
