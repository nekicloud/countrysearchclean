//
//  FlagImageInteractor.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class FlagImageInteractor: FlagImageUseCase {
    let provider: FlagProvider
    
    init(provider: FlagProvider) {
        self.provider = provider
    }
    
    func fetchFlag(for countryCode: String, completion: @escaping (Response<FlagImage>) -> Void) {
        provider.fetchFlag(for: countryCode, completion: {response in
            switch response{
            case .success(let imageData):
                completion(.success(FlagImage(code: countryCode, image: imageData)))
            case .error(let error):
                print(error.localizedDescription)
            }
        })
    }        
}
