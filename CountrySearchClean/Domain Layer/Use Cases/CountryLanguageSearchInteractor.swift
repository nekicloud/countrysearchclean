//
//  CountryLanguageSearchInteractor.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class CountryLanguageSearchInteractor: CountryLanguageSearchUseCase{
    let provider: CountryLanguageSearchProvider
    
    init(provider: CountryLanguageSearchProvider){
        self.provider = provider
    }
    
    func fetchCountry(for language: String, completion: @escaping (Response<Countries>) -> Void) {
        let languageCode = Locale.canonicalLanguageIdentifier(from: capitalizeOnlyFirst(string: language))
        provider.fetchCountry(for: languageCode, completion: completion)
    }
    
    private func capitalizeOnlyFirst(string: String) -> String{
        let capitalized = string.prefix(1).uppercased() + string.lowercased().dropFirst()
        return capitalized
    }
}
