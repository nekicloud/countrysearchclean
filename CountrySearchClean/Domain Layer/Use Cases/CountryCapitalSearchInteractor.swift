//
//  CountryCapitalInteractor.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class CountryCapitalSearchInteractor: CountryCapitalSearchUseCase{
    let provider: CountryCapitalSearchProvider
    
    init(provider: CountryCapitalSearchProvider){
        self.provider = provider
    }
    
    func fetchCountry(for capital: String, completion: @escaping (Response<Countries>) -> Void) {
        provider.fetchCountry(for: capital, completion: completion)
    }
}
