//
//  CountriesInteractor.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class CountriesInteractor: CountriesUseCase{
    let provider: CountriesProvider
    
    init(provider: CountriesProvider){
        self.provider = provider
    }
    
    func fetchCountriesByDistance(completion: @escaping (Response<Countries>) -> Void) {
        provider.fetchCountries(completion: completion)
    }
}
