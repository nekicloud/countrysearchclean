//
//  CountriesByDistanceUseCase.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol CountriesSorterUseCase{
    func sortCountries(countriesToSort: Countries) -> Countries
}
