//
//  FlagImage.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

struct FlagImage{
    var image: Data
    var countryCode: String
    
    init(code: String, image: Data){
        self.image = image
        self.countryCode = code
    }
}
