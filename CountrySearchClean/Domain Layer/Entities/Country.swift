//
//  Country.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol Country{
    var name: String {get}
    var countryCode: String {get}
    var population: Int {get}
    var capitalCity: String {get}
    var region: String{get}
    var regionBlocks: String{get}
    var language: String{get}
    var currency: String {get}
    var coordinates: Coordinates{get}
}

typealias Countries = [Country]
