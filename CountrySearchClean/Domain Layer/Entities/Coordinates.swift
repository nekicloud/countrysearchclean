//
//  LocationCode.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class Coordinates {
    var lat: Double
    var lon: Double
    
    init(lat: Double, lon: Double){
        self.lat = lat
        self.lon = lon
    }
}
