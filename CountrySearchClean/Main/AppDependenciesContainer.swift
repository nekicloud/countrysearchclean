//
//  AppDependenciesContainer.swift
//  CountrySearchClean
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class AppDependenciesContainer: CountrySearchVCDependencies, ResultVCDataSourceDependencies{
    var languageSearchUseCase: CountryLanguageSearchUseCase = CountryLanguageSearchInteractor(provider: RCLanguageSearchProvider())
    var capitalSearchUseCase: CountryCapitalSearchUseCase = CountryCapitalSearchInteractor(provider: RCCapitalSearchProvider())
    var nameSearchUseCase: CountryNameSearchUseCase = CountryNameSearchInteractor(provider: RCNameSearchProvider())
    var allCountriesUseCase: CountriesUseCase = CountriesInteractor(provider: RCCountriesProvider())
    var countriesSorter: CountriesSorterUseCase = CountriesSorterInteractor(provider: AppleLocationDistanceProvider())
    var flagFetcher: FlagImageUseCase = FlagImageInteractor(provider: FPFlagProvider())
}
