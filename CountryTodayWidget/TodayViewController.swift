//
//  TodayViewController.swift
//  CountryTodayWidget
//
//  Created by Luka on 29/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import NotificationCenter

@objc (TodayViewController)
class TodayViewController: UIViewController, NCWidgetProviding {
    @IBOutlet weak var flag: UIImageView!
    @IBOutlet weak var countryLbl: UILabel!
    let dependency:FlagImageUseCase = FlagImageInteractor(provider: FPFlagProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        countryLbl.text = UserDefaults(suiteName: "group.luka.sharedDefaults")!.string(forKey: "country")
        guard let countryCode = UserDefaults(suiteName: "group.luka.sharedDefaults")!.string(forKey: "countryCode") else {return}
        fetchImage(countryCode: countryCode)
        completionHandler(NCUpdateResult.newData)
    }
    
    func fetchImage(countryCode: String){
        dependency.fetchFlag(for: countryCode) {[weak self] response in
            switch response{
            case .success(let image):
                guard let flag = UIImage(data: image.image) else {return}
                self?.flag.image = flag
            case .error(let error):
                print(error)
            }
        }
    }
}
